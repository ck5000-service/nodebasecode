const db = require('app/models');
const { NotFoundError, BadRequestError } = require('app/libs/error');
const { sendEmailForgotPassword } = require('app/tasks/mail');

const create = async (data) => {
  const user = await db.user.create(data);
  return user;
};

const getOr404ByEmail = async (email) => {
  const user = await db.user.findOne({ email });
  if (!user) throw new NotFoundError('Email does not exist');
  return user;
};

const forgotPassword = async (email) => {
  const user = await getOr404ByEmail({ where: { email } });
  const setPasswordCode = await user.forgotPassword();
  await sendEmailForgotPassword(user, setPasswordCode);
};

const verifyPasswordSetPasswordCode = async (setPasswordCode) => {
  const user = await db.User.findOne({ where: { setPasswordCode } });
  if (!user) throw new BadRequestError('Code not match');
  if (!user.setPasswordCodeExpiryTime <= new Date()) throw new BadRequestError('Code has been expired');
  return user;
};

module.exports = {
  create,
  getOr404ByEmail,
  forgotPassword,
  verifyPasswordSetPasswordCode
};