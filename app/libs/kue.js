const kue = require('kue');
const { redis } = require('app/config');

const queue = kue.createQueue(redis);

const registerProcess = (event, amount, active) => {
  queue.process(event, amount, active);
};

const createJob = (event, data) => {
  const job = queue
    .create(event, data)
    .priority('high')
    .save(function(error) {
      if (error) return console.log(error);
      console.log(`Created job ${job.id}`);
    });

  job
    .on('complete', function(result) {
      console.log('Job completed with data ', result);
    })
    // eslint-disable-next-line no-unused-vars
    .on('failed attempt', function(errorMessage, doneAttempts) {
      console.log('Job failed');
    })
    // eslint-disable-next-line no-unused-vars
    .on('failed', function(errorMessage) {
      console.log('Job failed');
    })
    .on('progress', function(progress, data) {
      console.log(
        '\r  job #' + job.id + ' ' + progress + '% complete with data ',
        data
      );
    });
};

module.exports = {
  registerProcess,
  createJob
};
