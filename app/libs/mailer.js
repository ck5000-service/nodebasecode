const config = require('app/config');
const transporter = require('app/config/mail/transporter');

const send = async (to, subject, html) => {
  const info = await transporter.sendMail({
    from: config.mailer.NO_REPLY_MAIL, // sender address
    to, // list of receivers
    subject, // Subject line
    html // html body
  });
  console.log('Message sent: %s', info.messageId);
};

module.exports = { send };
