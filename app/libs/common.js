module.exports = {
  syncMiddleware: callback => (res, req, next) => {
    Promise.resolve(callback(res, req, next)).catch(next);
  }
};
