const { NotFoundError } = require('app/libs/error');

module.exports = (model) => {
  model.findOrFailById = async function(id){
    const instance = await this.findByPk(id);
    if (!instance) throw new NotFoundError(`${model.name} not found`);
    return instance;
  };
};