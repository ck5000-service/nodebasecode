const roundHalfDown = (number) => {
  return Math.floor(number * 2) / 2;
};

const roundHalfUp = (number) => {
  return Math.ceil(number * 2) / 2;
};

module.exports = {
  roundHalfDown,
  roundHalfUp
};