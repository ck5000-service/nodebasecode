class BaseError extends Error {
  constructor(message, statusCode) {
    super(message);

    this.statusCode = statusCode;
    this.status = `${statusCode}`.startsWith('4') ? 'fail' : 'error';
    this.isOperational = true;
    
    Error.captureStackTrace(this, this.constructor);
  }
}

class BadRequestError extends BaseError {
  constructor(message) {
    super(message, 400);
  }
}

class UnauthorizedError extends BaseError {
  constructor(message) {
    super(message, 401);
  }
}

class ForbiddenError extends BaseError {
  constructor(message) {
    super(message, 403);
  }
}

class NotFoundError extends BaseError {
  constructor(message) {
    super(message, 404);
  }
}

// eslint-disable-next-line no-unused-vars
const handleError = (err, req, res, next) => {
  console.log('ERROR: ' + err);
  err.statusCode = err.statusCode || 500;
  err.status = err.status || 'error';

  res.status(err.statusCode).json({
    status: err.status,
    message: err.message
  });
};

module.exports = {
  handleError,
  BadRequestError,
  UnauthorizedError,
  ForbiddenError,
  NotFoundError
};



