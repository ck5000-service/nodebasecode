const redis = require('redis');
const bluebird = require('bluebird');
const { redis: redisOptions } = require('app/config');
console.log('redis options ' + JSON.stringify(redisOptions));
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

const client = redis.createClient(redisOptions.url);

client.on('error', function(err) {
  console.log('Error ' + err);
  process.exit(0);
});

module.exports = {
  client
};
