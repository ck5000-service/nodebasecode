const jwt = require('jsonwebtoken');
const { secretKey } = require('app/config');

const generateToken = (userId, expiresIn = 3600) => {
  const token = jwt.sign({ data: userId }, secretKey, { expiresIn });
  return token;
};

const verifyToken = (token) => {
    const userId = jwt.verify(token, secretKey);
    return userId;
};

module.exports = {
  generateToken,
  verifyToken
};
