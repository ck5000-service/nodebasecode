const format = require('string-format');
const fs = require('fs');
const mailer = require('app/libs/mailer');
const { createJob, registerProcess } = require('app/libs/kue');

const registerEvent = () => {
  registerProcess('sendMail', 1, async (job, done) => {
    const { to, subject, message } = job.data;
    console.log(`====start send email: ${to}=====`);
    await mailer.send(to, subject, message);
    console.log(`====sent email: ${to}=====`);
    done();
  });
};

const sendEmailForgotPassword = async user => {
  const mailTemplate = fs
    .readFileSync('app/templates/forgotpassword.html')
    .toString();
  const message = format(mailTemplate, user);
  createJob('sendMail', {
    to: user.email,
    subject: 'Forgot Password',
    message
  });
};



module.exports = {
  registerEvent,
  sendEmailForgotPassword
};
