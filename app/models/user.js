'use strict';
const { hash, verify } = require('app/libs/crypto');
const { BadRequestError } = require('app/libs/error');
const { uuid } = require('uuidv4');

const ROLES = {
  admin: 'admin',
  customer: 'customer',
  owner: 'owner'
};

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('user', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      allowNull: false
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    status: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'ACTIVE'
    },
    passwordHash: {
      type: DataTypes.STRING,
      allowNull: true
    },
    role: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: ROLES.customer
    },

    setPasswordCode: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    setPasswordCodeExpiryTime: {
      type: DataTypes.DATE,
      allowNull: true,
    }
  },
  {
    sync: { force: true }
  }
  );
  User.prototype.setPassword = async function(password){
    this.passwordHash = hash(password);
    await this.save();
    return this;
  };

  User.prototype.checkPassword = function(password){
    return verify(password, this.passwordHash);
  };

  User.prototype.forgotPassword = async function(){
    this.setPasswordCode = uuid();
    this.setPasswordCodeExpiryTime = new Date(Date.now() + 60 * 60 * 1000); // 60 minutes
    await this.save();
    return this.setPasswordCode;
  };

  User.prototype.verifySetPasswordCode = async function(setPasswordCode){
    if (this.setPasswordCode !== setPasswordCode) throw new BadRequestError('Code does not match');
    if (this.setPasswordCodeExpiryTime <= new Date()) throw new BadRequestError('Code has been expired');
    return true;
  };

  User.prototype.toJSON =  function () {
    var values = Object.assign({}, this.get());
  
    delete values.passwordHash;
    delete values.setPasswordCode;
    delete values.setPasswordCodeExpiryTime;
    return values;
  };
  return User;
};