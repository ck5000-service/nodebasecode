const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const basename = path.basename(module.filename);
const env = process.env.NODE_ENV || 'development';
const config = require('app/config/sequelize')[env];
const baseQuery = require('app/libs/baseQuery');
const db = {};

const  sequelize = new Sequelize({
  ...config,
  dialect: 'postgres',
  benchmark: true,
  define: {
    underscored: true,
    paranoid:true
  },
  hooks: {
    afterConnect: () => console.log('Connected to database')
  }
});

fs
  .readdirSync(__dirname)
  .filter(file =>
    (file.indexOf('.') !== 0) &&
    (file !== basename) &&
    (file.slice(-3) === '.js'))
  .forEach(file => {
    const model = sequelize.import(path.join(__dirname, file));
    baseQuery(model);
    model.sync({ alter: true });
    if (model.name) db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;