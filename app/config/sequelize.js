require('dotenv').config();

module.exports = {
  'development': {
    'username': null,
    'password': null,
    'database': process.env.DATABASE,
    'host': '127.0.0.1',
    'dialect': 'postgres'
  },
  'test': {
    'username': null,
    'password': null,
    'database': process.env.DATABASE,
    'host': '127.0.0.1',
    'dialect': 'postgres'
  },
  'production': {
    'username': null,
    'password': null,
    'database': process.env.DATABASE,
    'host': '127.0.0.1',
    'dialect': 'postgres'
  }
};
