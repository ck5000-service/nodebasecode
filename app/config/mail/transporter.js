const nodemailer = require('nodemailer');
const config = require('app/config');

const transporter = nodemailer.createTransport({
  host: config.mailer.AWS_SMTP_HOST,
  // port: 569,
  auth: {
    user: config.mailer.AWS_SMTP_USERNAME,
    pass: config.mailer.AWS_SMTP_PASSWORD
  }
});

module.exports = transporter;
