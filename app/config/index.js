require('dotenv').config();

const config = {
  port: process.env.PORT,

  db: {
    url: process.env.DATABASE_URL,
    dialect: 'postgres',
  },

  redis: {
    url: process.env.REDIS_URL
  },

  jwt: {
    secret: process.env.JWT_SECRET,
    expiry: 3600000
  },

  stripeProduct: {
    expiry: 600
  },

  user: {
    role: {
      admin: 'admin',
      customer: 'customer',
      owner: 'owner'
    }
  },

  secretKey: process.env.SECRET_KEY,

  mailer: {
    AWS_SMTP_USERNAME: process.env.AWS_SMTP_USERNAME,
    AWS_SMTP_PASSWORD: process.env.AWS_SMTP_PASSWORD,
    AWS_SMTP_HOST: process.env.AWS_SMTP_HOST,
    NO_REPLY_MAIL: process.env.NO_REPLY_MAIL,
    HOST_NAME: process.env.HOST_NAME
  },

  groupType: {
    individual: 'individual',
    smallBusiness: 'small business',
    growthPlan: 'growth plan'
  },

  headCount: {
    individual: 1,
    smallBusiness: 5,
    growthPlan: 10
  },

  STRIPE_TEST_API_KEY: process.env.STRIPE_TEST_API_KEY,

  paymentType: {
    monthly: 'monthly',
    annually: 'annually'
  },

  gender: {
    male: 'male',
    female: 'female'
  },

  cognito: {
    USER_POOL_ID: process.env.AWS_COGNITO_USER_POOL_ID,
    CLIENT_ID: process.env.AWS_COGNITO_CLIENT_ID,
    REGION: process.env.AWS_COGNITO_REGION,
    ACCESS_KEY_ID: process.env.AWS_COGNITO_ACCESS_KEY_ID,
    SECRET_ACCESS_KEY: process.env.AWS_COGNITO_SECRET_ACCESS_KEY
  },

  registerEndPoint: process.env.LINK_REGISTER,
  clinic: {
    SHEET_NAME: 'Clinic_Profile_DataOnly',
    header: {
      Clinic_Name: 'name',
      Clinic_Slug: 'slug',
      Clinic_Type: 'type',
      Clinic_Qualification: 'qualification',
      Nurse_visit_price: 'nurseVisitPrice',
      Base_Price: 'basePrice',
      Premium_Price: 'premiumPrice',
      Street_Address: 'streetAddress',
      Suite: 'suite',
      City: 'city',
      Lat: 'latitude',
      Long: 'longitude',
      State: 'state',
      Zipcode: 'zipCode',
      Clinic_Phone: 'phone',
      Clinic_Website: 'website',
      Clinic_Notification_Phone: 'notificationPhone',
      Clinic_Notification_Email: 'notificationEmail',
      Clinic_Manager_Name: 'managerName',
      Clinic_Manager_Email: 'managerEmail',
      Clinic_Manager_Phone: 'managerPhone',
      Navigation_Instruction: 'navigationInstruction',
      Monday_Open: 'mondayOpen',
      Monday_Close: 'mondayClose',
      Tuesday_Open: 'tuesdayOpen',
      Tuesday_Close: 'tuesdayClose',
      Wednesday_Open: 'wednesdayOpen',
      Wednesday_Close: 'wednesdayClose',
      Thursday_Open: 'thursdayOpen',
      Thursday_Close: 'thursdayClose',
      Friday_Open: 'fridayOpen',
      Friday_Close: 'fridayClose',
      Saturday_Open: 'saturdayOpen',
      Saturaday_Close: 'saturadayClose',
      Sunday_Open: 'sundayOpen',
      Sunday_Close: 'sundayClose',
      Open_Holiday: 'openHoliday',
      Accept_Walk_Ins: 'acceptWalkIns',
      Accept_Scheduled_Appointments: 'acceptScheduledAppointments',
      Clinical_Exams: 'clinicalExams',
      Lab_Tests: 'labTests',
      X_rays: 'xRays',
      DOT_Physicals: 'dotPhysicals',
      Drug_Screen: 'drugScreen',
      Breath_Acohol_Test: 'breathAcoholTest',
      Auditory_Test: 'auditoryTest',
      Travel_Medicine: 'travelMedicine',
      Hydration_Therapy: 'hydrationTherapy',
      Dispense_Medication: 'dispenseMedication',
      Other: 'other'
    },
    services: [
      'clinicalExams',
      'labTests',
      'xRays',
      'Travel_Medicine',
      'drugScreen',
      'breathAcoholTest',
      'auditoryTest',
      'travelMedicine',
      'hydrationTherapy',
      'dispenseMedication'
    ],
    operations: [
      'mondayOpen',
      'mondayClose',
      'tuesdayOpen',
      'tuesdayClose',
      'wednesdayOpen',
      'wednesdayClose',
      'thursdayOpen',
      'thursdayClose',
      'fridayOpen',
      'fridayClose',
      'saturdayOpen',
      'saturadayClose',
      'sundayOpen',
      'sundayClose'
    ],
    schedulingLogics: [
      'openHoliday',
      'acceptWalkIns',
      'acceptScheduledAppointments'
    ]
  },
  xlsx: {
    type: [
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      'application/vnd.openxmlformats-officedocument.spreadsheetml.template',
      'application/vnd.ms-excel.sheet.macroEnabled.12',
      'application/vnd.ms-excel.template.macroEnabled.12',
      'application/vnd.ms-excel.addin.macroEnabled.12',
      'application/vnd.ms-excel.sheet.binary.macroEnabled.12'
    ]
  },
  slack: {
    hook:process.env.SLACK_HOOK
  }
};

module.exports = config;
