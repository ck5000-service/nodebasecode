const User = require('app/models').user;
const { verifyToken } = require('app/libs/jwt');

const authorized = roles => {
  return async (req, res, next) => {
    const tokenBearer = req.headers['authorization'];
    if (!tokenBearer.startsWith('Bearer ')) {
      return res.unauthorized('Auth token invalid');
    }
    const token = tokenBearer.slice(7, tokenBearer.length);
    try {
      const { data: userID } = verifyToken(token);
      const user = await User.findByPk(userID);
      req.credentials = { user, token };
      if (!user) return res.internalServerError('Require authentication');
      if (!roles.includes(user.role))
        return res.unauthorized(`Role ${user.role} is not allowed`);
      return next();
    } catch (error) {
      return res.unauthorized(error.message);
    }
  };
};

module.exports = authorized;
