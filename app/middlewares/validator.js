const Joi = require('joi');

const validator = schema => {
  return async (req, res, next) => {
    const result = Joi.validate(req.body, schema);
    if (result.error !== null) {
      return res.badRequest(result.error.message);
    }
    return next();
  };
};

module.exports = validator;
