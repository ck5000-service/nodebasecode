const Joi = require('joi');
const baseSchema = require('app/libs/baseSchema');

module.exports = {
  updateSchema: Joi.object().keys({
    name: Joi.string().strip().optional()
  }),
  createSchema: Joi.object().keys({
    name: Joi.string().strip(),
    email: baseSchema.emailSchema
  })
};
