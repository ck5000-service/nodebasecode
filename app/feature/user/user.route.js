const router = require('express').Router();
const { syncMiddleware } = require('app/libs/common');
const validator = require('app/middlewares/validator');
const userController = require('./user.controller');
const userValidation = require('./user.validation');
const authorized = require('app/middlewares/authorized');
const {
  user: { role: userRole }
} = require('app/config');

router.get(
  '',
  syncMiddleware(authorized(Object.values(userRole))),
  syncMiddleware(userController.getList)
);

router.post(
  '',
  syncMiddleware(authorized(Object.values(userRole))),
  syncMiddleware(validator(userValidation.createSchema)),
  syncMiddleware(userController.create)
);

router.put(
  '/me',
  syncMiddleware(authorized(Object.values(userRole))),
  syncMiddleware(validator(userValidation.updateSchema)),
  syncMiddleware(userController.updateMe)
);

router.get(
  '/me',
  syncMiddleware(authorized(Object.values(userRole))),
  syncMiddleware(userController.getMe)
);

router.get(
  '/:id',
  syncMiddleware(authorized(Object.values(userRole))),
  syncMiddleware(userController.getOne)
);

router.put(
  '/:id',
  syncMiddleware(authorized(Object.values(userRole))),
  syncMiddleware(validator(userValidation.updateSchema)),
  syncMiddleware(userController.update)
);

router.delete(
  '/:id',
  syncMiddleware(authorized(Object.values(userRole))),
  syncMiddleware(userController.delete)
);

module.exports = router;
