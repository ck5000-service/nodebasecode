const userService = require('app/services/user');
const User = require('app/models').user;

module.exports = {
  getList: async (req, res) => {
    const { query: { 
      limit = 10 , 
      offset = 0, 
      orderBy = 'createdAt', 
      orderType = 'DESC'
    } } = req;
    const where = {};
    const order = [
      [orderBy, orderType]
    ];
    const { rows: users, count } = await User.findAndCountAll({ where, limit, offset, order });
    return res.ok({
      items: users, 
      count,
      offset,
      limit
    });
  },

  getOne: async (req, res) => {
    const {
      params: { id }
    } = req;
    const user = await User.findOrFailById(id);
    return res.ok(user);
  },

  create: async (req, res) => {
    const {
      body: data
    } = req;
    const user = await userService.create(data);
    return res.ok(user);
  },

  update: async (req, res) => {
    const {
      params: { id },
      body: data
    } = req;
    const user = await User.findOrFailById(id);
    await user.update(data);
    return res.ok(user);
  },

  delete: async (req, res) => {
    const {
      params: { id },
    } = req;
    const user = await User.findOrFailById(id);
    await user.destroy();
    return res.ok({});
  },

  getMe: async (req, res) => {
    const {
      credentials: { user }
    } = req;
    return res.ok(user);
  },

  updateMe: async (req, res) => {
    const {
      credentials: { user },
      body: data
    } = req;
    await user.update(data);
    return res.ok(user);
  },
};