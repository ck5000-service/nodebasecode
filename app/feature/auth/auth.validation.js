const Joi = require('joi');
const baseSchema = require('app/libs/baseSchema');

module.exports = {
  loginSchema: Joi.object().keys({
    email: baseSchema.emailSchema.required(),
    password: baseSchema.passwordSchema.required()
  }),

  registerSchema: Joi.object().keys({
    name: Joi.string().strip(),
    email: baseSchema.emailSchema,
    password: baseSchema.passwordSchema
  }),

  changePasswordSchema: Joi.object().keys({
    oldPassword: baseSchema.passwordSchema,
    password: baseSchema.passwordSchema
  }),

  forgotPasswordSchema: Joi.object().keys({
    email: baseSchema.emailSchema.required()
  }),

  resetPasswordSchema: Joi.object().keys({
    email: baseSchema.emailSchema.required(),
    resetPasswordCode: Joi.string().strip().required(),
    password: baseSchema.passwordSchema.required()
  })
};
