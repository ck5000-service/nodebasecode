const router = require('express').Router();
const { syncMiddleware } = require('app/libs/common');
const validator = require('app/middlewares/validator');
const authController = require('./auth.controller');
const authValidation = require('./auth.validation');
const authorized = require('app/middlewares/authorized');
const {
  user: { role: userRole }
} = require('app/config');

router.post(
  '/register',
  syncMiddleware(validator(authValidation.registerSchema)),
  syncMiddleware(authController.register)
);

router.post(
  '/login',
  syncMiddleware(validator(authValidation.loginSchema)),
  syncMiddleware(authController.login)
);

router.post(
  '/change-password',
  syncMiddleware(authorized(Object.values(userRole))),
  syncMiddleware(validator(authValidation.changePasswordSchema)),
  syncMiddleware(authController.changePassword)
);

router.post(
  '/forgot-password',
  syncMiddleware(validator(authValidation.forgotPasswordSchema)),
  syncMiddleware(authController.forgotPassword)
);

router.post(
  '/reset-password',
  syncMiddleware(validator(authValidation.resetPasswordSchema)),
  syncMiddleware(authController.resetPassword)
);

module.exports = router;
