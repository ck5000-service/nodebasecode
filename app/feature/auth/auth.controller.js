const userService = require('app/services/user');
const { BadRequestError } = require('app/libs/error');
const { generateToken } = require('app/libs/jwt');

module.exports = {
  register: async (req, res) => {
    const { body: { password, ...data } } = req;
    const user = await userService.create(data);
    await user.setPassword(password);
    return res.ok(user);
  },

  login: async (req, res) => {
    const { body: { password, email } } = req;
    const user = await userService.getOr404ByEmail(email);
    if (!user.checkPassword(password)) {
      throw BadRequestError('Password is incorrect');
    }
    const token = generateToken(user.id);
    return res.ok({
      user,
      token
    });
  },

  changePassword: async (req, res) => {
    const {
      body: { oldPassword, password },
      credentials: { user }
    } = req;
    if (!user.checkPassword(oldPassword)) {
      throw BadRequestError('Password is incorrect');
    }
    await user.setPassword(password);
    return res.ok(user);
  },

  forgotPassword: async (req, res) => {
    const { body: { email } } = req;
    await userService.forgotPassword(email);
    return res.ok();
  },

  resetPassword: async (req, res) => {
    const { body: { password, setPasswordCode } } = req;
    const user = await userService.verifyPasswordSetPasswordCode(setPasswordCode);
    await user.setPassword(password);
    return res.ok('Change password success.');
  }
};
