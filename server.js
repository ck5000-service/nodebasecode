const express = require('express');
const http = require('http');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const logger = require('morgan');
const helmet = require('helmet');
const baseResponse = require('app/libs/baseResponse');
const { handleError } = require('app/libs/error');
const config = require('app/config');
const swagger = require('app/config/swagger');
const cors = require('cors');
const app = express();
const server = http.createServer(app);

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(helmet());
app.use(cookieParser());
app.use(bodyParser.json());
app.use(baseResponse);
app.use(cors());

swagger(app);
app.use('', require('app/feature'));
app.use((req, res) => { res.notFound('not found'); });
app.use(handleError);

try {
  server.listen(config.port);
  console.log(`server listen at ${config.port}`);
} catch (error) {
  console.log(`server listen err ${error}`);
  process.exit(0);
}

require('./app/cronjob');

module.exports = app;
